import datetime

import requests

ticker = 'BTC-USD'
# https://docs.pro.coinbase.com/#get-historic-rates
url = 'https://api.pro.coinbase.com/products/{}/candles'.format(ticker)
ticker = ticker.replace('-', '')

start = datetime.datetime(2015, 7, 20)  # first day for BTCUSD data on Coinbase

count = 0
with open("{}_data.csv".format(ticker), "a") as fp:  # TODO add downloading only missing data
    while start < datetime.datetime.now():
        query_params = {
            'granularity': 60,  # in seconds, i.e. 1 minute
            'start': start.isoformat(),
            'end': start + datetime.timedelta(minutes=300)  # max per request
        }

        response = requests.get(url, params=query_params)
        if response.status_code != 200:
            print("error {} {}".format(url, response))
        else:
            count += 1
            if count % 100 == 0:
                print(start.isoformat())

            start += datetime.timedelta(minutes=301)  # to prevent overlapping
            str_data = []
            data = response.json()
            for row in data[::-1]:  # by default data come in desc by date (latest first) -> therefore we reverse
                row_datetime = datetime.datetime.utcfromtimestamp(row[0]).strftime("%Y-%m-%d %H:%M")
                str_row = [ticker, row_datetime]
                str_row += [str(item) for item in row[1:]]
                str_data.append(",".join(str_row))
            fp.writelines("%s\n" % row for row in str_data)  # symbol, datetime, low, high, open, close, volume
fp.close()
